'use strict';
// Load properties
var PropertiesReader = require('properties-reader');
var properties = PropertiesReader('public/config/mongo-collections.js');
// Imports
var mongodb = require('mongodb');
var assert = require('assert');
// Instantiations
var MongoClient = mongodb.MongoClient;
var url = 'mongodb://'+ properties.get('mongo-db-host') +':'+ properties.get('mongo-db-port') +'/' + properties.get('mongo-db-name') ;
var url;
var collection;

class MongoConnection{
	
	constructor()
	{			
	}
	
	// Insert a message in collection
	insertMessageInCollection(message, messagesCollection,callbackFunction)
	{		
		console.log("Message to push ###############" + message + " to collection "+ messagesCollection);
		MongoClient.connect(url, function(err, db) {
			assert.equal(null, err);
			db.collection(messagesCollection).insert( message, function(err, result) {
				assert.equal(err, null);
				console.log('Inserted a message into collection ' + messagesCollection);
				callbackFunction(true);
				db.close();
			});
		});
	}
	
	//search functionality by prateek starts
	search(searchString, configurationCollection, callbackFunction)
	{		
		var searchStatus = '';
		var botArray = [];
		console.log('Searching for the string : ' + searchString);
		MongoClient.connect(url, function(err, db) {
			assert.equal(null, err);
			//Fetch the search result
			
			if(searchString!=null && searchString!='' && searchString!=' ')	{
				db.collection(configurationCollection).find({"meta-tag" : new RegExp(searchString, 'i')}).toArray(function (err, result) {
					console.log("Search string not equal to null, empty, space");
					if(result != null && result != '')	{
						searchStatus='success';
						for(var i=0;i<result.length;i++)	{
							botArray[i]=result[i].botname;
						}
					}
					else 
					{
						searchStatus='failure';
						botArray[0] = "none";
						console.log('No search bot');
						
					}
					
				var answer = new Object();
				answer.searchStatus= searchStatus;
				answer.botArray = botArray;
				callbackFunction(answer);
					
				});
			}	else	{
				db.collection(configurationCollection).find().toArray(function (err, result) {
					console.log("When string empty, null, space");
					if(result != null && result != '')	{
						searchStatus='success';
						for(var i=0;i<result.length;i++)	{
							botArray[i]=result[i].botname;
							console.log("Bot Array------>"+botArray[i]);
						}
					}
					else 
					{
						searchStatus='failure';
						botArray[0] = "none";
						console.log('No search bot');
						
					}
					
				var answer = new Object();
				answer.searchStatus= searchStatus;
				answer.botArray = botArray;
				callbackFunction(answer);
				});
				
			}
			//Close connection
			db.close();
			
		});
	}
}	

module.exports.MongoConnection = MongoConnection;
